# Functional Asymmetry

Analysis of functional asymmetry for the Nature Human Behaviour manuscript.

~~Most recently run on **14/02/2022** to account for changes in surface transforms after reviewer comments.~~

Most recently run on **23/02/2022** becasue incorrect surface transforms was used.

## Procedure

1. Resample the native func surface to the updated dhcp32kSym mesh for each subject
    - `hcp_surface.sh`
    - `batch_hcp_surface.sh`
2. Run MIGP on these surfaces
    - `migp.ipynb`
3. Run symmetric ICA 
    - `symmetric_ICA.ipynb`
    - Split and concatenate the MIGP hemispheres
    - Run ICA on the concatenated hemispheres
    - Duplicate the ICA into the missing hemisphere
4. Run DR
    - `dual_regression.ipynb`