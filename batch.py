#!/usr/bin/env python
"""
batch

batch run a script over subid/sesid desribed in <batch-file> csv.
"""
import argparse
import pandas as pd
from subprocess import check_output
import os
import os.path as op
import glob

class ExistsException(Exception):
    pass

class MissingException(Exception):
    pass

def _run_batch(cmd, batch, exclude_missing=None, exclude_exists=None, dry_run=True):
    
#     print(exclude_missing)
#     print(exclude_exists)
    
    batch = pd.read_csv(batch, dtype={'subid': str, 'sesid': str})
    
    
    for subid, sesid in zip(batch.subid, batch.sesid):
        
        args = dict(
            subid=subid,
            sesid=sesid,
        )
        
# #         if exclude_missing is not None and not op.exists(exclude_missing.format(**args)):
# #             print("exclude missing: " + exclude_missing.format(**args))
# #             continue
        
#         if exclude_missing is not None and not (len(glob.glob(exclude_missing.format(**args)))>0):
#             print("exclude missing: " + exclude_missing.format(**args))
#             continue
            
#         if exclude_exists is not None and op.exists(exclude_exists.format(**args)):
#             print("exclude exists: " + exclude_exists.format(**args))
#             continue

        if exclude_missing is not None:
            try:
                if not isinstance(exclude_missing, list):
                    exclude_missing = [exclude_missing]
                for l in exclude_missing:
                    if not (len(glob.glob(l.format(**args))) > 0):
                        print("exclude missing: " + l.format(**args))
                        raise MissingException()
            except MissingException:
                continue
            
        if exclude_exists is not None:
            try:
                if not isinstance(exclude_exists, list):
                    exclude_exists = [exclude_exists]
                for l in exclude_exists:
                    if (len(glob.glob(l.format(**args))) > 0):
                        print("exclude exists: " + l.format(**args))
                        raise ExistsException()
            except ExistsException:
                continue
            
        if dry_run:
            print(cmd.format(**args))
        else:
            print(run(cmd.format(**args)))
      
    
def run(cmd):

    if type(cmd) is list:
        str = " ".join(cmd)
    else:
        str = cmd
        cmd = str.split()

    jobout = check_output(cmd)
    return jobout.decode('utf-8').strip()




if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument('batch', help='batch filename')
    parser.add_argument('cmd', help='shell command to execute')
    parser.add_argument('--exclude_missing', default=None, help='exclude if missing', action='append')
    parser.add_argument('--exclude_exists', default=None, help='exclude if exists', action='append')
    parser.add_argument('--dry_run', action='store_true', help='just print commands (do not execute)')
    
    args = parser.parse_args()
    _run_batch(**vars(args))