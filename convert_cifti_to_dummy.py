#!/usr/bin/env python
import nibabel as nb
import sys
import numpy as np

def fold(d):
    
    n_t = d.shape[1]
    n_v = d.shape[0]
    
    dim = np.ceil(n_v / 100.0).astype(int)
    
    d0 = np.zeros((100 * dim, n_t))
    d0[:n_v, :] = d
    d0 = d0.reshape((10, 10, dim, n_t))
    
    mask = np.ones((100 * dim))
    mask[n_v:] = 0
    mask = mask.reshape((10, 10, dim))
    
    return d0, mask.astype(np.int16)


f = sys.argv[1]
nifti_name = sys.argv[2]

d0, m0 = fold(nb.load(f).get_fdata(caching='unchanged').T)

nb.Nifti1Image(d0, affine=np.eye(4)).to_filename(nifti_name + '.dummy.nii.gz')
nb.Nifti1Image(m0, affine=np.eye(4)).to_filename(nifti_name + '_mask.dummy.nii.gz')
