#!/bin/bash

batch_file="/homes/sfitzgib/dhcp-results/neofmri_3rd_release/subject-info/fmri_to_process_FINAL.csv"

workdir="/homes/sfitzgib/dhcp-results/neofmri_3rd_release/workdir"

dry_run="--dry_run"
if [ "$1" == "run" ]; then
   dry_run=""
fi

./batch.py $dry_run \
    $batch_file \
    "fsl_sub -R 22000 -T 2880 -l logs/logs.hcpsurf -N {subid}-{sesid}-hcp-surface ./hcp_surface.sh {subid} {sesid} $workdir" \
    --exclude_missing="/vol/dhcp-results/neofmri_3rd_release/asymmetry_analysis/surface_transforms/sub-{subid}_ses-{sesid}_hemi-left_from-native_to-dhcpSym40_dens-32k_mode-sphere.reg40.surf.gii" \
    --exclude_exists="${workdir}/sub-{subid}/ses-{sesid}/hcp_surface/func_mesh-dhcp32kSym_space-extdhcp40wk_desc-cortexSmooth4mm.dtseries.nii"
    
#     --exclude_exists="${workdir}/sub-{subid}/ses-{sesid}/surface/func_mesh-dhcp32kSym_space-extdhcp40wk.dtseries.nii" #\
#     --exclude_missing="${workdir}/sub-{subid}/ses-{sesid}/reg/func-mcdc_to_struct/func-mcdc_to_struct_affine.mat" \
#     --exclude_missing="${workdir}/sub-{subid}/ses-{sesid}/denoise/func_clean.nii.gz" 