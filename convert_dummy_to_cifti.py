#!/usr/bin/env python
import nibabel as nb
import sys
import numpy as np

def nifti_to_cifti(nifti, mask, ref_cifti, outname, dtype='dtseries'):

    dd = nb.load(nifti)
    d0 = dd.get_fdata()[nb.load(mask).get_fdata().astype(bool)]

    cifti = nb.load(ref_cifti)
    
    bm_full = cifti.header.get_axis(1)
    
    if dtype == 'dtseries':
        series = nb.cifti2.SeriesAxis(start=0, step=1, size=d0.shape[-1])
    elif dtype == 'dscalar':
        series = nb.cifti2.ScalarAxis(name=[f'{i}' for i in np.arange(d0.shape[-1])])
    else:
        raise RuntimeError(f'Unknown dtype: {dtype}')
    
    header = nb.cifti2.Cifti2Header.from_axes((series, bm_full))
    nb.Cifti2Image(d0.T, header=header).to_filename(outname)
    
    
if __name__ == '__main__':
    
    nifti = sys.argv[1]    
    mask = sys.argv[2]
    ref = sys.argv[3]
    outname = sys.argv[4]
    dtype = sys.argv[5]
    
    nifti_to_cifti(nifti, mask, ref, outname, dtype)

